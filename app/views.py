# -*- coding: utf-8 -*-

import json
import os
import urllib2
import slugify
from BeautifulSoup import BeautifulSoup
from rest_framework.decorators import api_view, parser_classes
from rest_framework.response import Response
from rest_framework.parsers import JSONParser
from webkit2png.webkit2png import WebkitRenderer, QApplication, QTimer
from urlparse import urlparse

from django.views.generic import TemplateView, FormView
from django.shortcuts import redirect
from django.core.urlresolvers import reverse_lazy
from django.contrib import messages
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.forms import AuthenticationForm
from django.conf import settings
from django.views.decorators.csrf import csrf_exempt


from .models import Page, Category
from .forms import RegisterForm, LoginForm, ForgotPassForm, CategoryForm


class Index(FormView):
    template_name = 'login.html'
    # form_class = LoginForm
    form_class = AuthenticationForm
    success_url = reverse_lazy('home')

    def get_context_data(self, **kwargs):
        context = super(Index, self).get_context_data(**kwargs)
        return context

    def form_valid(self, form):
        login(self.request, form.get_user())

        return super(Index, self).form_valid(form)


class Main(TemplateView):
    template_name = 'index.html'

    def get_context_data(self, **kwargs):
        context = super(Main, self).get_context_data(**kwargs)
        user = self.request.user
        categories = user.categories.all()
        query = self.request.POST.get('query')
        pages = user.pages
        if query:
            pages = pages.filter(title__icontains=query)
        pages = pages.all()
        context.update({
            'categories': categories,
            'pages': pages,
            'user_id': self.request.user.id})

        return context

    def post(self, request, *args, **kwargs):
        return self.get(request, *args, **kwargs)


class Register(FormView):
    template_name = 'register.html'
    form_class = RegisterForm
    success_url = reverse_lazy('register')

    def get_context_data(self, **kwargs):
        context = super(Register, self).get_context_data(**kwargs)

        return context

    def form_valid(self, form):
        if form.save():
            messages.success(
                self.request,
                'Registration completed. You will receive an email as soon as the manager confirms your account.')
        else:
            for error in form.error_messages:
                print error
                messages.error(self.request, error['err_msg'])
            return self.form_invalid(form)
        return super(Register, self).form_valid(form)


class Forgot(FormView):
    template_name = 'forgot_password.html'
    form_class = ForgotPassForm

    def get_context_data(self, **kwargs):
        context = super(Forgot, self).get_context_data(**kwargs)
        return context


@api_view(['GET', 'POST'])
def get_user(request):
    user = User.objects.get(pk=request.GET.get('id'))
    return Response(user.username)


@api_view(['GET', 'POST'])
def register(request):
    resp = {}
    if request.method == 'POST':
        username = request.POST.get('username')
        email = request.POST.get('email')
        password = request.POST.get('password')
        user = User(username=username, email=email, password=password)
        try:
            user.save()
        except Exception:
            resp.update({'error': Exception.message})
            return Response(resp, status=500)
        return Response(status=200)
    else:
        return Response(status=404)


@api_view(['GET', 'POST'])
def logout_user(request):
    resp = {}
    try:
        logout(request)
    except Exception as e:
        resp.update({'error': e.message})
        return Response(resp, status=500)
    return redirect('home')


@api_view(['POST'])
@parser_classes((JSONParser,))
def add_category(request):
    if request.method == 'POST':
        category_name = request.POST.get('name')
        category = Category(name=category_name, user=request.user)
        category.save()
        return Response(json.dumps({
            'category_id': category.id,
        }))
    else:
        return Response(status=403)


@api_view(['POST'])
@parser_classes((JSONParser,))
def rename_category(request):
    if request.method == 'POST':
        category_id = request.POST.get('id')
        new_name = request.POST.get('name')
        try:
            category = Category.objects.get(pk=category_id)
            category.name = new_name
            category.save()
        except Category.DoesNotExist:
            # TODO(analytic): add handler code
            raise
        return Response(json.dumps({}))
    else:
        return Response(status=403)


@api_view(['POST'])
@parser_classes((JSONParser,))
def delete_category(request):
    if request.method == 'POST':
        category_id = request.POST.get('id')
        category = Category.objects.get(pk=category_id)
        category.delete()
        return Response(json.dumps({}))
    else:
        return Response(status=403)


def load_page_snapshot(page_url, category_id, user_id):
    webpage = urllib2.urlopen(page_url).read()
    soup = BeautifulSoup(''.join(webpage))
    user = User.objects.get(pk=user_id)
    category = Category.objects.get(id=category_id)
    title = soup('title')
    if len(title) > 0:
        title = title[0].string
        page_title = slugify.slugify_ru(title)
    else:
        parsed_uri = urlparse(page_url)
        page_title = parsed_uri.netloc

    file_name = slugify.slugify_ru(u''.join(e for e in page_title if e.isalnum())) + '.png'

    page = Page.objects.create(title=page_title,
                               url=page_url,
                               image_file=file_name,
                               category=category,
                               user=user)

    path = page.image_path()
    if not os.path.exists(os.path.dirname(path)):
        os.makedirs(os.path.dirname(path))
    os.system('xvfb-run -a -s "-screen 0 640x480x16" webkit2png -T -o %s %s' % (
        path,
        page_url))
    # TODO(analytic): save only when image was successfully created ?

    return page


@api_view(['POST'])
@parser_classes((JSONParser,))
def add_page(request):
    if request.method == 'POST':
        page_url = request.POST.get('url')
        category_id = request.POST.get('category')

        page = load_page_snapshot(page_url, category_id, user_id=request.user.id)

        img_url = page.image_url()
        try:
            return Response(json.dumps({
                'img_url': img_url,
                'url': page_url,
                'page_id': page.id,
                'title': page.title,
                'cat_name': page.category.name,
            }))
        except Exception as e:
            print e
            raise
    else:
        return Response(status=403)


@api_view(['POST'])
@parser_classes((JSONParser,))
def delete_page(request):
    if request.method == 'POST':
        page_id = request.POST.get('id')
        page = Page.objects.get(pk=page_id)
        page.delete()
        return Response(json.dumps({}))
    else:
        return Response(status=403)
