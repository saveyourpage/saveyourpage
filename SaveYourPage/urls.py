from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static

from app.decorators import is_user, is_guest
from app import views
admin.autodiscover()

urlpatterns = patterns('',
    url(r'^$', is_user(views.Main.as_view()), name='home'),
    url(r'^login', is_guest(views.Index.as_view()), name='login'),
    url(r'^register', is_guest(views.Register.as_view()), name='register'),
    url(r'^forgot_password', is_guest(views.Forgot.as_view()), name='forgot'),
    # url(r'^add_page/', views.add_page, name='add-page'),
    url(r'^ajax/add_page/', views.add_page, name='add_page'),
    url(r'^ajax/delete_page/', views.delete_page, name='delete_page'),
    url(r'^ajax/add_category/', views.add_category, name='add_category'),
    url(r'^ajax/rename_category/', views.rename_category, name='rename_category'),
    url(r'^ajax/delete_category/', views.delete_category, name='delete_category'),
    url(r'^get_user/', views.get_user, name='get_user'),
    url(r'^reg_user/', views.register, name='register_user'),
    url(r'^log_out/', views.logout_user, name='log_out'),

    # url(r'^blog/', include('blog.urls')),
    # (r'^static/(?P<path>.*)$', 'django.views.static.serve',
    # {'document_root': '/Users/air13/Desktop/SYP/saveyourpage/static'}),

    url(r'^admin/', include(admin.site.urls)),
) + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
